﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MyFashions1Nov
{
    /// <summary>
    /// Interaction logic for AppDetails.xaml
    /// </summary>
    public partial class AppDetails : UserControl
    {
        public event EventHandler myEventBacktoAdminSetting;
        public event EventHandler startManualSync;
        Configuration config;
        public AppDetails()
        {
            InitializeComponent();
        }

        private void btnBacktoAdminSetting_TouchDown_1(object sender, TouchEventArgs e)
        {
            myEventBacktoAdminSetting(this, null);
        }

        private void btnSave_TouchDown_1(object sender, TouchEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtSellerApiKey.Text.Trim()) && !string.IsNullOrEmpty(txtSellerEmailid.Text.Trim()) && !string.IsNullOrEmpty(txtSellerid.Text.Trim()))
            {
                this.Cursor = Cursors.Wait;

                config.AppSettings.Settings["SellerID"].Value = txtSellerid.Text;
                config.AppSettings.Settings["SellerEmail"].Value = txtSellerEmailid.Text;
                config.AppSettings.Settings["APIKey"].Value = txtSellerApiKey.Text;
                config.Save();
                
                try
                {
                    startManualSync(this, null);
                    myEventBacktoAdminSetting(this, null);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "myfashions", MessageBoxButton.OK, MessageBoxImage.Information);
                    if (ex.Message.ToString().Trim() == "Please check authorization details.")
                    {
                        txtSellerid.IsReadOnly = false;
                        txtSellerEmailid.IsReadOnly = false;
                        txtSellerApiKey.IsReadOnly = false;
                        txtSellerid.Text = string.Empty;
                        txtSellerEmailid.Text = string.Empty;
                        txtSellerApiKey.Text = string.Empty;
                    }
                }

                this.Cursor = Cursors.Arrow;
            }
            else
            {
                if (string.IsNullOrEmpty(txtSellerid.Text.Trim()))
                {
                    MessageBox.Show("Seller ID: Required field is empty.", "myfashions", MessageBoxButton.OK, MessageBoxImage.Information);
                    txtSellerid.Focus();
                }
                else if (string.IsNullOrEmpty(txtSellerEmailid.Text.Trim()))
                {
                    MessageBox.Show("Seller Email: Required field is empty.", "myfashions", MessageBoxButton.OK, MessageBoxImage.Information);
                    txtSellerEmailid.Focus();
                }
                else if (string.IsNullOrEmpty(txtSellerApiKey.Text.Trim()))
                {
                    MessageBox.Show("Seller API Key: Required field is empty.", "myfashions", MessageBoxButton.OK, MessageBoxImage.Information);
                    txtSellerApiKey.Focus();
                }
            }
        }

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
            if (!string.IsNullOrEmpty(config.AppSettings.Settings["SellerID"].Value) && !string.IsNullOrEmpty(config.AppSettings.Settings["SellerID"].Value) && !string.IsNullOrEmpty(config.AppSettings.Settings["SellerID"].Value))
            {
                txtSellerid.Text = config.AppSettings.Settings["SellerID"].Value;
                txtSellerEmailid.Text = config.AppSettings.Settings["SellerEmail"].Value;
                txtSellerApiKey.Text = config.AppSettings.Settings["APIKey"].Value;
            }

            //txtSellerApiKey.IsReadOnly = true;
            //txtSellerEmailid.IsReadOnly = true;
            //txtSellerid.IsReadOnly = true;
        }

        private void btnCancel_TouchDown_1(object sender, TouchEventArgs e)
        {
            myEventBacktoAdminSetting(this, null);
        }
    }
}
