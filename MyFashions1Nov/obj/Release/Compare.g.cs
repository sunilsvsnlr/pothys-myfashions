﻿#pragma checksum "..\..\Compare.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "A83975289B4A236DB4684D3B492DE9CB"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using MyFashions1Nov;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using WpfCamera;


namespace MyFashions1Nov {
    
    
    /// <summary>
    /// Compare
    /// </summary>
    public partial class Compare : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 82 "..\..\Compare.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid contentPresenter;
        
        #line default
        #line hidden
        
        
        #line 93 "..\..\Compare.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnClearAll;
        
        #line default
        #line hidden
        
        
        #line 105 "..\..\Compare.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Media.RotateTransform AnimatedRotateTransform3;
        
        #line default
        #line hidden
        
        
        #line 126 "..\..\Compare.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid canvas1;
        
        #line default
        #line hidden
        
        
        #line 131 "..\..\Compare.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgDragtoCart;
        
        #line default
        #line hidden
        
        
        #line 146 "..\..\Compare.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgDragtoCmp;
        
        #line default
        #line hidden
        
        
        #line 161 "..\..\Compare.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid Canvas2;
        
        #line default
        #line hidden
        
        
        #line 167 "..\..\Compare.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCart;
        
        #line default
        #line hidden
        
        
        #line 175 "..\..\Compare.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnDelete;
        
        #line default
        #line hidden
        
        
        #line 213 "..\..\Compare.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox LstParent;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/MyFashions1Nov;component/compare.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\Compare.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 9 "..\..\Compare.xaml"
            ((MyFashions1Nov.Compare)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Window_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.contentPresenter = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            this.BtnClearAll = ((System.Windows.Controls.Button)(target));
            
            #line 93 "..\..\Compare.xaml"
            this.BtnClearAll.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.BtnClearAll_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 4:
            this.AnimatedRotateTransform3 = ((System.Windows.Media.RotateTransform)(target));
            return;
            case 5:
            this.canvas1 = ((System.Windows.Controls.Grid)(target));
            
            #line 126 "..\..\Compare.xaml"
            this.canvas1.PreviewTouchMove += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.Canvas1_PreviewTouchMove_1);
            
            #line default
            #line hidden
            
            #line 126 "..\..\Compare.xaml"
            this.canvas1.PreviewTouchUp += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.Canvas1_PreviewTouchUp_1);
            
            #line default
            #line hidden
            return;
            case 6:
            this.imgDragtoCart = ((System.Windows.Controls.Image)(target));
            return;
            case 7:
            this.imgDragtoCmp = ((System.Windows.Controls.Image)(target));
            return;
            case 8:
            this.Canvas2 = ((System.Windows.Controls.Grid)(target));
            return;
            case 9:
            this.btnCart = ((System.Windows.Controls.Button)(target));
            return;
            case 10:
            this.btnDelete = ((System.Windows.Controls.Button)(target));
            return;
            case 11:
            this.LstParent = ((System.Windows.Controls.ListBox)(target));
            
            #line 210 "..\..\Compare.xaml"
            this.LstParent.ManipulationBoundaryFeedback += new System.EventHandler<System.Windows.Input.ManipulationBoundaryFeedbackEventArgs>(this.LstParent_ManipulationBoundaryFeedback_1);
            
            #line default
            #line hidden
            
            #line 211 "..\..\Compare.xaml"
            this.LstParent.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.LstParent_TouchDown_1);
            
            #line default
            #line hidden
            
            #line 212 "..\..\Compare.xaml"
            this.LstParent.PreviewTouchUp += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.LstParent_PreviewTouchUp_1);
            
            #line default
            #line hidden
            
            #line 212 "..\..\Compare.xaml"
            this.LstParent.PreviewTouchMove += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.LstParent_PreviewTouchMove_2);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

