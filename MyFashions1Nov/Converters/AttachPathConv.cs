﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace MyFashions1Nov.Converters
{
    class AttachPathConv:IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            //return  @"\StoreImages\" + value.ToString();
            if (System.IO.File.Exists(Environment.CurrentDirectory + @"\StoreImages\" + value.ToString()))
            {
                Uri uriret = new Uri(Environment.CurrentDirectory + @"\StoreImages\" + value.ToString());
                return uriret;
            }
            else
            {
                Uri uriret = new Uri(Environment.CurrentDirectory + @"\StoreImages\sample.png");
                return uriret;
            }

            //return "pack://application:,,/StoreImages/" + value.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
