﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows.Threading;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Media.Animation;

namespace MyFashions1Nov
{
    /// <summary>
    /// Interaction logic for CategoryUC.xaml
    /// </summary>
    public partial class CategoryUC : UserControl
    {
        public CreateDBEntity.Models.MainCategory Categ;
        public CreateDBEntity.Models.SubCategory MainSubCateg;
        public CreateDBEntity.Models.Category SubCateg;
        public CreateDBEntity.Models.productTemplateBeans ptBean;
        public CreateDBEntity.Models.Store Store;
        public myCustomItemsControl listing;
        public CategoryUC()
        {
            InitializeComponent();
            txtCount.Text = "-0.33";
        }

        public event MyeventHandler MyEventHand;
        public event MyeventHandler MyEventStackPanel;
        public delegate void MyeventHandler(object sender, EventArgs e);
        
        public string CountIndex(int val)
        {
            decimal retValue = (decimal)-0.28;
            switch (val)
            {
                case 1:
                    retValue = (decimal)-0.25;
                    break;
                case 2:
                    retValue = (decimal)-0.29;
                    break;
                case 3:
                    retValue = (decimal)-0.33;
                    break;
                case 4:
                    retValue = (decimal)-0.37;
                    break;
                case 5:
                    retValue = (decimal)-0.42;
                    break;
                case 6:
                    retValue = (decimal)-0.46;
                    break;
                case 7:
                    retValue = (decimal)-0.47;
                    break;
                case 8:
                    retValue = (decimal)-0.55;
                    break;
                case 9:
                    retValue = (decimal)-0.565;
                    break;
                case 10:
                    retValue = (decimal)-0.58;
                    break;
                case 11:
                    retValue = (decimal)-0.67;
                    break;
                case 12:
                    retValue = (decimal)-0.68;
                    break;
                default:
                    retValue = (decimal)-0.58;
                    break;
            }
            return retValue.ToString();
        }


        public void Refresh()
        {
            try
            {
                LogHelper.Logger.Info("Category Refresh Starts");
                Categ = this.DataContext as CreateDBEntity.Models.MainCategory;
                txtCount.Text = CountIndex(this.categoryContext.Items.Count);
                MyEventStackPanel(this, null);
                LogHelper.Logger.Info("Category Refresh Ends");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: CategoryUC : Refresh: " + ex.Message, ex);
                myfashionsecurity.AuthenticationCodeLogic.sendLogEMail("C:/myfashions/Logs/myfashionApp_" + DateTime.Now.ToString("MMddyyyy"));
            }            
        }

        private void BtnItem_TouchDown(object sender, TouchEventArgs e)
        {
            try
            {
                Button btn = sender as Button;
                Categ = btn.DataContext as CreateDBEntity.Models.MainCategory;
                SubCateg = btn.DataContext as CreateDBEntity.Models.Category;
                LogHelper.Logger.Info("When ButtonItem TouchDown Starts");
                if (Categ != null)
                {
                    CategFill(Categ, btn);
                }
                else if (SubCateg != null)
                {
                    if (SubCateg.subCategory.ToList().Exists(c => c.Name.Equals("Sarees", StringComparison.OrdinalIgnoreCase)))
                    {
                        CreateDBEntity.Models.SubCategory subSareesCateg = SubCateg.subCategory.Where(c => c.Name.Equals("Sarees", StringComparison.OrdinalIgnoreCase)).SingleOrDefault();
                        if (subSareesCateg != null)
                        {
                            SubCateg.subCategory.Remove(subSareesCateg);
                            List<CreateDBEntity.Models.FabricsInfo> lstFabrics = subSareesCateg.ProdTemplateBeans.GroupBy(c => c.fabric).Select(c => c.Key).ToList();
                            List<CreateDBEntity.Models.SubCategory> lstReturnCateg = new List<CreateDBEntity.Models.SubCategory>();
                            if (lstFabrics != null)
                            {
                                if (lstFabrics.Exists(c => c.Name.Equals("silk", StringComparison.OrdinalIgnoreCase)))
                                {
                                    CreateDBEntity.Models.FabricsInfo fabSilk = lstFabrics.Where(c => c.Name.Equals("silk", StringComparison.OrdinalIgnoreCase)).SingleOrDefault();
                                    CreateDBEntity.Models.SubCategory subCategSilkOnly = FillSubCategOnlySilk(subSareesCateg, fabSilk);
                                    SubCateg.subCategory.Add(subCategSilkOnly);
                                    CreateDBEntity.Models.SubCategory subCategExceptSilk = FillSubCategsExceptSilk(lstFabrics, subSareesCateg);
                                    SubCateg.subCategory.Add(subCategExceptSilk);
                                }
                            }
                        }
                    }
                    SubCategFill(SubCateg, btn);
                }
                else
                {
                    LoadItemsControl(btn);
                }
                LogHelper.Logger.Info("When ButtonItem TouchDown Stops");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: CategoryUCPage : BtnItem_TouchDown: " + ex.Message, ex);
                myfashionsecurity.AuthenticationCodeLogic.sendLogEMail("C:/myfashions/Logs/myfashionApp_" + DateTime.Now.ToString("MMddyyyy"));
            }
        }

        private CreateDBEntity.Models.SubCategory FillSubCategOnlySilk(CreateDBEntity.Models.SubCategory subSareesCateg, CreateDBEntity.Models.FabricsInfo fabSilk)
        {
            CreateDBEntity.Models.SubCategory subCategSilkOnly = new CreateDBEntity.Models.SubCategory();
            subCategSilkOnly.id = subSareesCateg.id + 1;
            subCategSilkOnly.ProdTemplateBeans = subSareesCateg.ProdTemplateBeans.Where(d => d.fabric.id.Equals(fabSilk.id)).ToList();
            subCategSilkOnly.Name = fabSilk.Name;
            subCategSilkOnly.check = true;
            subCategSilkOnly.success = true;
            subCategSilkOnly.category = subSareesCateg.category;
            return subCategSilkOnly;
        }

        private void SubCategFill(CreateDBEntity.Models.Category SubCateg, Button btn)
        {
            if (SubCateg.subCategory != null)
            {
                stkPnlMainHeading.DataContext = SubCateg.subCategory;
                categoryContext.ItemsSource = SubCateg.subCategory.Where(c => c.check == true);
                txtCount.Text = CountIndex(categoryContext.Items.Count);//SubCateg.subCategory.Count);
                stkPnlMainHeading.DataContext = SubCateg;

                Storyboard MyStoryboard = FindResource("CtgListingSB") as Storyboard;
                MyStoryboard.Stop();
                RenderTransform = new MatrixTransform(0.9, 0, 0, 0.9, 1, 10);
            }
            else
            {
                LoadItemsControl(btn);
            }
        }

        private void CategFill(CreateDBEntity.Models.MainCategory Categ, Button btn)
        {
            if (Categ.lstCategory.Count == 1)
            {
                stkPnlMainHeading.DataContext = Categ.lstCategory[0];
                categoryContext.ItemsSource = Categ.lstCategory[0].subCategory.Where(c => c.check == true).OrderBy(c => c.id).ToList();
                txtCount.Text = CountIndex(categoryContext.Items.Count);// Categ.lstCategory[0].subCategory.Count);
            }
            else
            {
                stkPnlMainHeading.DataContext = Categ.lstCategory;
                categoryContext.ItemsSource = Categ.lstCategory.Where(c => c.check == true).OrderBy(c => c.id).ToList();

                txtCount.Text = CountIndex(categoryContext.Items.Count); //CountIndex(Categ.lstCategory.Count);
            }
            MyEventStackPanel(this, null);
            stkPnlMainHeading.DataContext = Categ;
        }

        private void LoadItemsControl(Button btn)
        {
            Storyboard MyStoryboard = (Storyboard)this.FindResource("CtgListingSB") as Storyboard;
            MyStoryboard.Begin();

            MyEventHand(btn.DataContext, null);
        }

        private CreateDBEntity.Models.SubCategory FillSubCategsExceptSilk(List<CreateDBEntity.Models.FabricsInfo> lstFabrics, CreateDBEntity.Models.SubCategory subSareesCateg)
        {
            List<CreateDBEntity.Models.productTemplateBeans> lstProdTempBeans = new List<CreateDBEntity.Models.productTemplateBeans>();
            lstFabrics.Where(c => !c.Name.Equals("silk", StringComparison.OrdinalIgnoreCase)).ToList().ForEach(c =>
                {
                    lstProdTempBeans.AddRange(subSareesCateg.ProdTemplateBeans.Where(d => d.fabric.id.Equals(c.id)));
                });
            CreateDBEntity.Models.SubCategory subCategSilk = new CreateDBEntity.Models.SubCategory();
            subCategSilk.check = true;
            subCategSilk.success = true;
            subCategSilk.Name = "Others";
            subCategSilk.category = subSareesCateg.category;
            subCategSilk.ProdTemplateBeans = lstProdTempBeans;
            subCategSilk.id = 1001;
            return subCategSilk;
        }

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {

        }
    }
}
