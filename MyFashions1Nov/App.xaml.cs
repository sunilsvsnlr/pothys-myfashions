﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using myfashionsClientSDK.Security;
namespace MyFashions1Nov
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup_1(object sender, StartupEventArgs e)
        {
            if (e.Args.Length.Equals(1))
            {
                LogHelper.Logger.Info("TouchOnCloud" + e.Args[0] + "TouchOnCloud");
                ApplicationSecurity appSecurity = new ApplicationSecurity();
                if (appSecurity.CheckApplicationAccess(e.Args[0]))
                {
                    SplashScreen splashScreen = new SplashScreen();
                    splashScreen.Show();
                }
                else
                {
                    MessageBox.Show("Invalid Application Launch. Please launch using CMC or contact software vendor.", "Pothys", MessageBoxButton.OK, MessageBoxImage.Information);
                    this.Shutdown();
                }
            }
            else
            {
                MessageBox.Show("Invalid Application Launch. Please launch using CMC or contact software vendor.", "Pothys", MessageBoxButton.OK, MessageBoxImage.Information);
                this.Shutdown();
            }
        }
    }
}
