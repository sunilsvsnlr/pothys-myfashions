﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MyFashions1Nov
{
    /// <summary>
    /// Interaction logic for DockUserControl.xaml
    /// </summary>
    public partial class DockUserControl : UserControl
    {
        public DockUserControl()
        {
            InitializeComponent();
        }

        public event myeventHandler myDockLoadSubCateg;
        public delegate void myeventHandler(object sender, EventArgs e);

        private void CheckBox_Checked_1(object sender, RoutedEventArgs e)
        {
            CheckBox chk = sender as CheckBox;
            var data = chk.DataContext;
            myDockLoadSubCateg(chk, null);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

        }
    }
}
